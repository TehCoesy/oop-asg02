package IO;

import Core.Vector2i;

import java.awt.*;
import java.awt.geom.Rectangle2D;

public class MyButton {
    public Color bodyColor = Color.RED, textColor = Color.WHITE;
    public Vector2i pos,size;
    public String text;
    public int text_size = 40;
    public int border = 4;

    private Font font = new Font("TimesRoman", Font.PLAIN, text_size);
    private FontMetrics fontMetrics;

    private Vector2i stringSize = new Vector2i();
    private Vector2i padding = new Vector2i(4, 4);

    public MyButton(Vector2i pos, Vector2i size) {
        this.pos = pos; this.size = size;

    }

    public void highlight() {
        this.textColor = Color.YELLOW;
    }

    public void unhighlight() {
        this.textColor = Color.WHITE;
    }

    public void render(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;

        //Border
        g2d.setColor(Color.WHITE);
        g2d.setStroke(new BasicStroke(border));
        g2d.drawRect(this.pos.getX(), this.pos.getY(), this.size.getX(), this.size.getY());

        //ButtonFill
        g.setColor(bodyColor);
        g.fillRect(this.pos.getX() + border / 2, this.pos.getY() + border / 2, this.size.getX() - border, this.size.getY() - border);

        //Text
        g.setColor(textColor);
        g.setFont(font);

        if (fontMetrics == null) {
            fontMetrics = g.getFontMetrics();

            Rectangle2D stringBox = fontMetrics.getStringBounds(this.text, g);

            int X = (int) stringBox.getWidth();
            int Y = (int) stringBox.getHeight();

            this.stringSize.set(X,Y);
        }

        g.drawString(text, this.pos.getX() + padding.getX(), this.pos.getY() + stringSize.getY() + padding.getY());
    }

    private void getTextPosition() {

    }
}
