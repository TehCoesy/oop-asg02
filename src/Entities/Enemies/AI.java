package Entities.Enemies;

import java.util.Random;
import Entities.LocomotiveEntity;
import Entities.Player;
import static States.GameSetting.BLOCK_SIZE;

public class AI {
    Player p;
    Enemy e;
    public int orient , direction ;
    /*    public int getRandom(Random rand, int start, int end, int... exclude){
            int random = start + rand.nextInt(end - start + 1 - exclude.length);
            for (int ex : exclude){
                if (random < ex){
                    break;
                }
                random++;
            }
            return random;
        }
    */
    public void set(Player player, Enemy orneal) {
        p = player;
        e = orneal;
    }

    public int PlayerFound() {
        //System.out.println(p.getRelativePosition().getX() + " " + p.getRelativePosition().getY());
        //System.out.println(e.getRelativePosition().getX() + " " + e.getRelativePosition().getY());

        if (PlayerChaseCollum() != -1) {
            return PlayerChaseCollum();
        } else if (PlayerChaseRow() != -1) {
            return PlayerChaseRow();
        }
        //System.out.println();

        return  -1;
        // else orient = getRandom(rand, 0, 3);
    }


    protected int PlayerChaseCollum() {

        if (p.getRelativePosition().getX() == e.getRelativePosition().getX()) {
            //System.out.println("Column");
            if (p.getRelativePosition().getY() < e.getRelativePosition().getY() && e.getRelativePosition().getY() - p.getRelativePosition().getY() <= 4) {
                return 1;
            } else if (p.getRelativePosition().getY() > e.getRelativePosition().getY() && p.getRelativePosition().getY() - e.getRelativePosition().getY() <= 4) {
                return 0;
            } else {
                return -1;
            }
        }

        //Đừng copy ngát thiếu dấu đóng mở ngoặc :)) dễ nhầm lắm
        return -1;
    }

    protected int PlayerChaseRow() {
        if (p.getRelativePosition().getY() == e.getRelativePosition().getY()) {
            //System.out.println("Row");
            if (p.getRelativePosition().getX() < e.getRelativePosition().getX() && e.getRelativePosition().getX() - p.getRelativePosition().getX() <= 4) {
                return 2;
            } else if (p.getRelativePosition().getX() > e.getRelativePosition().getX() && p.getRelativePosition().getX() - e.getRelativePosition().getX() <= 4) {
                return 3;
            } else {
                return -1;
            }
        }
        return -1;
    }
}






