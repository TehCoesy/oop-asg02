package Entities.Enemies;

import Container.GameEntities;
import Core.GameOverseer;
import Entities.Player;
import Graphics.SpriteBuilder;
import Entities.LocomotiveEntity;
import Entities.Enemies.AI;

import java.util.Random;

public class Oneal extends Enemy {
    private boolean moveEnemy;
    private int orient = 0;
    public AI ai = new AI();
    public int move;
    public Oneal() {
        this.setTolerance(0);
        this.setUpSprite(SpriteBuilder.getOnealSprite0());
        this.setDownSprite(SpriteBuilder.getOnealSprite1());
        this.setLeftSprite(SpriteBuilder.getOnealSprite2());
        this.setRightSprite(SpriteBuilder.getOnealSprite3());
        this.setDeadSprite(SpriteBuilder.getMobDeadSprite());
    }

    @Override
    public void update() {
        if (GLOBAL_TICKS % 15 == 0) {
            if (ai.PlayerFound() != -1) {
                moveEnemy = false;
                int orient = ai.PlayerFound();
                System.out.println(orient);
                switch (orient) {
                    case 0: moveDown(); break;
                    case 1: moveUp(); break;
                    case 2: moveLeft(); break;
                    case 3: moveRight(); break;
                }
            } else {
                Random rand = new Random();
                orient = getRandom(rand, 0 , 3);
                moveEnemy = true;
            }
        }

        if (moveEnemy) {
            if (orient == 0) {
                moveDown();
            }
            if (orient == 2) {
                moveLeft();
            }
            if (orient == 3) {
                moveRight();
            }
            if (orient == 1) {
                moveUp();
            }
        }
    }

    @Override
    public void updateAI() {
        switch (ai.PlayerFound()) {
            case 0: if (!collide_down) {
                moveDown();
                break;
            }
            case 1: if (!collide_top) {
                moveUp();
                break;
            }
            case 2: if (!collide_left) {
                moveLeft();
                break;
            }
            case 3: if (!collide_right) {
                moveRight();
                break;
            }
        }
    }

    //Bên ô thử đi
}
